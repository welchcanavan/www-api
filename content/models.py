"""
content models
"""

import mistune
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe
from dirtyfields import DirtyFieldsMixin
from django.core.files.base import ContentFile

# Image variations
from io import BytesIO
from PIL import Image as PillowImage, ImageOps


markdown = mistune.Markdown()


# Shortlinks
import requests
import os


# upload directory

import hashlib
import datetime


# Image Types
class Image(DirtyFieldsMixin, models.Model):
    date_hash = hashlib.md5(f"{datetime.date.today()}".encode('utf-8')).hexdigest()[:10]
    file_path = f"uploads/{date_hash}"

    caption = models.CharField(max_length=500)
    height = models.IntegerField()
    related_post = models.ForeignKey(
        "Post", on_delete=models.CASCADE, blank=True, null=True, related_name="images"
    )
    related_work = models.ForeignKey(
        "Work", on_delete=models.CASCADE, blank=True, null=True, related_name="images"
    )
    thumbnail = models.ImageField(null=True, upload_to=file_path)
    thumbnail_webp = models.ImageField(null=True, upload_to=file_path)
    upload = models.ImageField(height_field="height", width_field="width", upload_to=file_path)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    webp = models.ImageField(null=True, upload_to=file_path)
    width = models.IntegerField()

    def set_thumbnail(self):
        original_image = PillowImage.open(self.upload)
        filename = f"{self.upload.name.split('.')[0]}.thumbnail.jpg"
        image_io = BytesIO()
        image = ImageOps.fit(original_image, (500, 500), PillowImage.ANTIALIAS)
        image = image.convert(mode="RGB")
        image.save(image_io, "JPEG", quality=70, optimize=True, progressive=True)

        self.thumbnail.save(
            filename,
            ContentFile(image_io.getvalue()),
            save=False
        )

    def set_thumbnail_webp(self):
        original_image = PillowImage.open(self.upload)
        filename = f"{self.upload.name.split('.')[0]}.thumbnail.webp"
        image_io = BytesIO()
        image = ImageOps.fit(original_image, (500, 500), PillowImage.ANTIALIAS)
        image.save(image_io, "WEBP", quality=70, method=6)

        self.thumbnail_webp.save(
            filename,
            ContentFile(image_io.getvalue()),
            save=False
        )

    def set_webp(self):
        original_image = PillowImage.open(self.upload)
        filename = f"{self.upload.name.split('.')[0]}.webp"
        image_io = BytesIO()
        image = original_image
        image.save(image_io, "WEBP", quality=70, method=6)

        self.webp.save(
            filename,
            ContentFile(image_io.getvalue()),
            save=False
        )

    def save(self, *args, **kwargs):
        if not self.pk or 'upload' in self.get_dirty_fields():
            self.set_thumbnail()
            self.set_thumbnail_webp()
            self.set_webp()

        super().save(*args, **kwargs)

    class Meta:
        db_table = "image"

    def preview(self):
        url = self.upload.url

        if url:
            return mark_safe(
                '<img class="image-preview" src="{0}">'.format(self.upload.url)
            )

        return None

    def __str__(self):
        return self.caption


# Content Types
class ContentBase(models.Model):
    """Base Content Abstract Model"""

    description = models.TextField(blank=True, null=True)
    hero = models.OneToOneField(Image, blank=True, null=True, on_delete=models.CASCADE)
    markdown = models.TextField()
    published_at = models.DateTimeField("date published", default=timezone.now)
    slug = models.SlugField(primary_key=True)
    title = models.CharField(max_length=500)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title

    def body(self):
        return markdown(self.markdown)


class Post(DirtyFieldsMixin, ContentBase):
    DRAFT = "draft"
    PUBLISHED = "published"
    POST_STATUS_CHOICES = ((DRAFT, "Draft"), (PUBLISHED, "Published"))

    shortlink = models.CharField(max_length=20, null=True, blank=True, default='')
    status = models.CharField(max_length=20, choices=POST_STATUS_CHOICES, default=DRAFT)

    class Meta:
        db_table = "post"

    def set_shortlink(self):
        url = "https://api-ssl.bitly.com/v4/shorten"
        headers = {
            "Host": "api-ssl.bitly.com",
            "Accept": "application/json",
            "Authorization": f"Bearer {os.environ.get('BITLY_TOKEN')}"
        }
        payload = {
            "group_guid": os.environ.get('BITLY_GROUP_GUID'),
            "long_url": f"https://welchcanavan.com/{self.slug}"
        }
        r = requests.post(url, headers=headers, json=payload)

        if r.status_code is 200:
            self.shortlink = r.json()[u'id']

    def save(self, *args, **kwargs):
        if not self.shortlink or 'slug' in self.get_dirty_fields():
            self.set_shortlink()

        super().save(*args, **kwargs)

class Work(ContentBase):
    ART = "art"
    CODE = "code"
    WORK_TYPE_CHOICES = ((CODE, "Code"), (ART, "Art"))

    type = models.CharField(max_length=20, choices=WORK_TYPE_CHOICES, default=None)

    class Meta:
        db_table = "work"
