from rest_framework import serializers
from .models import Image, Post, Work

class ThumbnailField(serializers.Field):
    def to_representation(self, value):
        return {
            "src": value.thumbnail.url,
            "height": value.thumbnail.height,
            "width": value.thumbnail.width,
            "webp_src": value.thumbnail_webp.url
        }

class ImageSerializer(serializers.ModelSerializer):
    # alt_imgs = AlternateImagesField(source='*')
    thumbnail = ThumbnailField(source='*')
    webp_src = serializers.CharField(source='webp.url', read_only=True)
    src = serializers.CharField(source='upload.url', read_only=True)

    class Meta:
        model = Image
        fields = ("src", "caption", "width", "height", "webp_src", "thumbnail")


class BaseContentSerializer(serializers.HyperlinkedModelSerializer):
    hero = ImageSerializer()
    images = ImageSerializer(many=True)
    body = serializers.CharField()


class PostSerializer(BaseContentSerializer):
    class Meta:
        model = Post
        fields = (
            "body",
            "hero",
            "published_at",
            "shortlink",
            "slug",
            "title",
            "url",
        )


class WorkSerializer(BaseContentSerializer):
    class Meta:
        model = Work
        exclude = ("markdown")
